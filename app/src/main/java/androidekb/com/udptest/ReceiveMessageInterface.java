package androidekb.com.udptest;

public interface ReceiveMessageInterface {
    void onMessageReceived(ChatMessage chatMessage);
}
