package androidekb.com.udptest.Util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonUtil {

    public static <T> String convertModelJsonString(T object) {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
        String json = gson.toJson(object);
        return json;
    }

    public static <T>  T convertJsonToObject(String textJson, Class<T> type) {
        T result;
        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
        result = gson.fromJson(textJson, type);
        return result;
    }
}
