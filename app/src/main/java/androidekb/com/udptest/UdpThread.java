package androidekb.com.udptest;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

import androidekb.com.udptest.Util.JsonUtil;

public class UdpThread extends Thread {
    private boolean running;
    UDPHelper mUDPHelper;
    Context mContext;
    static UdpThread mUdpThread;

    String mMessage = "";
    ReceiveMessageInterface mReceiveMessageInterface;


    private UdpThread() {}

    public static synchronized UdpThread getInstance() {
        if (mUdpThread == null)
            mUdpThread = new UdpThread();
        return mUdpThread;
    }

    @Override
    public void run() {
        try {
            mUDPHelper = new UDPHelper(mContext, new UDPHelper.BroadcastListener() {
                @Override
                public void onReceive(String msg, String ip) {
                    if (msg.length()==0) return;
                    ChatMessage chatMessage = JsonUtil.convertJsonToObject(msg, ChatMessage.class);
                    if (chatMessage!=null)
                        mReceiveMessageInterface.onMessageReceived(chatMessage);
                }
            });
            mUDPHelper.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        running = true;
        while (running) {
            try {
                if (mMessage.length()>0) {
                    mUDPHelper.send(mMessage);
                }

                sleep(100);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void end() {
        running = false;
        mUDPHelper.end();
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public ReceiveMessageInterface getReceiveMessageInterface() {
        return mReceiveMessageInterface;
    }

    public void setReceiveMessageInterface(ReceiveMessageInterface receiveMessageInterface) {
        mReceiveMessageInterface = receiveMessageInterface;
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }
}
