package androidekb.com.udptest;

import com.google.gson.annotations.Expose;

public class ChatMessage {
    @Expose
    String name;
    @Expose
    String time;
    @Expose
    String text;

    public ChatMessage(String name, String time, String text) {
        this.name = name;
        this.time = time;
        this.text = text;
    }
}
