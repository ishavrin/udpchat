package androidekb.com.udptest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.Iterator;
import java.util.LinkedHashMap;

import androidekb.com.udptest.Util.JsonUtil;

import static spark.Spark.get;

public class MainActivity extends AppCompatActivity implements ReceiveMessageInterface {
TextView mTV;
EditText mETName;
EditText mETMessage;
String mMessage = "";
ScrollView mScrollView;
LinkedHashMap<String,String> mMap = new LinkedHashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTV = findViewById(R.id.text_view);
        mTV.setMovementMethod(new ScrollingMovementMethod());
        mETName = findViewById(R.id.editTextName);
        mETMessage = findViewById(R.id.editTextMessage);
        mScrollView = findViewById(R.id.scroll_view);
        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = mETName.getText().toString();
                String message = mETMessage.getText().toString();
                String time =System.currentTimeMillis()+"";
                if (message.length() == 0) return;
                mMessage = JsonUtil.convertModelJsonString(new ChatMessage (name,time,message));
                UdpThread.getInstance().setMessage(mMessage);
                mETMessage.setText("");
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        UdpThread.getInstance()
                .setContext(this.getApplicationContext());

        UdpThread.getInstance()
                .setReceiveMessageInterface(this);

        if (!UdpThread.getInstance().isRunning())
            UdpThread.getInstance().start();

    }

    public void updateChatMessages () {
        String text ="";
        Iterator<String> it = mMap.values().iterator();
        while (it.hasNext())
        {
            text = text + it.next() +"\n";

        }
        mTV.setText(text);
        mScrollView.fullScroll(View.FOCUS_DOWN);
    }

    @Override
    public void onMessageReceived(ChatMessage chatMessage) {
        String val = mMap.get(chatMessage.time);
        if (val == null) {
            mMap.put(chatMessage.time, chatMessage.name + ": " + chatMessage.text);
            (MainActivity.this).runOnUiThread(new Runnable() {
                public void run() {
                    updateChatMessages();
                }
            });
        }
    }

}
